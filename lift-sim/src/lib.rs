pub struct Lift {
    position: i64,
    destination: Option<i64>,
}

impl Lift {
    pub fn new(position: i64) -> Lift {
        Lift {
            position,
            destination: None,
        }
    }

    pub fn position(&self) -> i64 {
        self.position
    }

    pub fn set_destination(&mut self, destination: i64) {
        self.destination = Some(destination);
    }

    pub fn tick(&mut self) {
        match self.destination {
            Some(destination) => {
                self.position += destination.cmp(&self.position) as i64;
            }
            _ => (),
        };
    }
}

#[cfg(test)]
mod tests {
    use super::Lift;

    #[test]
    fn starts_at_given_position() {
        let lift = Lift::new(4);

        assert_eq!(4, lift.position());
    }

    #[test]
    fn moves_up_if_below_destination() {
        let mut lift = Lift::new(4);

        lift.set_destination(6);
        lift.tick();

        assert_eq!(5, lift.position());
    }

    #[test]
    fn moves_down_if_above_destination() {
        let mut lift = Lift::new(4);

        lift.set_destination(1);
        lift.tick();

        assert_eq!(3, lift.position());
    }

    #[test]
    fn does_not_move_if_at_destination() {
        let mut lift = Lift::new(4);

        lift.set_destination(4);
        lift.tick();

        assert_eq!(4, lift.position());
    }
}
