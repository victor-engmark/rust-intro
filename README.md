# rust-intro

[![Build Status](https://gitlab.com/victor-engmark/rust-intro/badges/master/build.svg)](https://gitlab.com/victor-engmark/rust-intro/pipelines/)

Nothing to see here, just [learning Rust](https://doc.rust-lang.org/book/).
