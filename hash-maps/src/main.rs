use std::collections::HashMap;

fn main() {
    basic_hashmap();
    hashmap_from_arrays();
    hashmap_ownership();
    value_retrieval();
    value_update();
    stats();
}

fn basic_hashmap() {
    let mut votes = HashMap::new();
    votes.insert("Duke Nukem", 1_000_000);
    votes.insert("Superman", 3);
    println!("Scores: {:?}", votes);
}

fn hashmap_from_arrays() {
    let contestants = vec!["Batman", "G-Man"];
    let votes = vec![50, 1];
    let contestant_votes: HashMap<_, _> = contestants.iter().zip(votes.iter()).collect();
    println!("Scores: {:?}", contestant_votes);
}

fn hashmap_ownership() {
    let key = String::from("key");
    let value = String::from("value");
    let mut map = HashMap::new();

    map.insert(key, value);

    println!("Now key and value belong to map");

    let copyable_key = 1;
    let copyable_value = 2;
    let mut map_with_copies = HashMap::new();
    map_with_copies.insert(copyable_key, copyable_value);
    println!(
        "Key {} and value {} were *copied* into the map",
        copyable_key, copyable_value
    );
}

#[derive(Debug)]
enum Pet {
    Dog,
    Rabbit,
}

fn value_retrieval() {
    let mut pets = HashMap::new();
    pets.insert("Fluffy", Pet::Dog);
    pets.insert("Bunnsy", Pet::Rabbit);

    for (key, value) in &pets {
        println!("{} is a {:?}", key, value);
    }
}

fn value_update() {
    let mut results = HashMap::new();

    results.insert("first", 10);
    results.insert("second", 5);
    results.insert("second", 4);
    println!("Results after update: {:?}", results);

    results.entry("third").or_insert(8);
    results.entry("second").or_insert(99);

    println!("Results after only inserting new: {:?}", results);

    let mut tag_cloud = HashMap::new();
    let text = "She sells sea-shells on the sea-shore.
The shells she sells are sea-shells, I'm sure.
For if she sells sea-shells on the sea-shore
Then I'm sure she sells sea-shore shells.";

    for word in text.split_whitespace() {
        let count = tag_cloud.entry(word).or_insert(0);
        *count += 1;
    }
    println!("{:?}", tag_cloud);
}

fn stats() {
    let mut values = vec![
        3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 6, 8, 9, 7, 9, 3, 2, 3, 8, 4, 6
    ];
    values.sort();

    let mut sum = 0;
    let mut value_count = HashMap::new();

    for &value in values.iter() {
        sum += value;
        let count = value_count.entry(value).or_insert(0);
        *count += 1;
    }

    let number_of_values = values.len();
    let mean = sum / number_of_values;

    let mut mode = 0;
    let mut max_count = 0;
    for (value, count) in value_count {
        if count > max_count {
            mode = value;
            max_count = count;
        }
    }

    let even_number_of_values = number_of_values % 2 == 0;
    let median_index = number_of_values / 2;
    let median;

    if even_number_of_values {
        median = (values[median_index] + values[median_index - 1]) / 2;
    } else {
        median = values[median_index];
    }

    println!("Mean is {}, median is {}, mode is {}", mean, median, mode);
}
