fn main() {
    println!("Hello, world!");
    more(2, 0.35);
}

fn cast_to_float(number: i32) -> f64 {
    number as f64
}

fn more(count: i32, rate: f64) {
    let interest = rate * 100.0;

    let return_payment = {
        let count = cast_to_float(count);
        count + (count * rate)
    };

    let prefix = if rate <= 0.5 {
        "OK"
    } else if rate <= 1.0 {
        "Too expensive"
    } else {
        "Loan shark"
    };

    println!(
        "{}; {} I'll be paying {} at {}%.",
        prefix, count, return_payment, interest
    );
}
