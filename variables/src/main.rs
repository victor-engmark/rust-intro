const AMPERE: u32 = 10;

fn main() {
    let volts = 100;
    println!("{} watts, shooting through your brain", volts * AMPERE);
    let volts = volts * 2;
    println!("{} watts, down the drain", volts * AMPERE);

    let examples: (char, i64) = ('😻', 999);
    let (heart_eyed_cat, _) = examples;
    println!("{} × {}", heart_eyed_cat, examples.1);

    let months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];

    println!("{} is the official drinking month", months[5]);
}
