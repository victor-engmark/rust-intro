pub struct Rectangle {
    pub width: u32,
    pub height: u32,
}

impl Rectangle {
    pub fn intersects(&self, other: &Rectangle) -> bool {
        if self.width == other.width || self.height == other.height {
            return true;
        }

        let wider = self.width > other.width;
        let taller = self.height > other.height;
        wider ^ taller
    }

    pub fn area(&self) -> u32 {
        self.width * self.height
    }

    fn validate(&self) {
        if self.width == 0 {
            panic!("Width cannot be zero");
        }
        if self.height == 0 {
            panic!("Height cannot be zero");
        }
    }
}

#[cfg(test)]
mod rectangle_intersects_tests {
    use super::Rectangle;

    #[test]
    #[should_panic(expected = "cannot be zero")]
    fn zero_width_rectangle() {
        let invalid = Rectangle {
            width: 0,
            height: 1,
        };
        invalid.validate();
    }

    #[test]
    #[should_panic(expected = "cannot be zero")]
    fn zero_height_rectangle() {
        let invalid = Rectangle {
            width: 1,
            height: 0,
        };
        invalid.validate();
    }
}
