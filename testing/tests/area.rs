extern crate testing;

mod common;

#[test]
fn area_of_unit_square() {
    let rect = testing::Rectangle {
        width: 1,
        height: 1,
    };
    assert_eq!(1, rect.area());
}

#[test]
fn area_of_non_square() {
    let rect = testing::Rectangle {
        width: 3,
        height: 2,
    };
    assert_eq!(6, rect.area());
}
