extern crate testing;

mod common;

#[test]
fn tall_rectangle_intersects_wide_rectangle() {
    common::setup();

    let tall = testing::Rectangle {
        width: 1,
        height: 2,
    };
    let wide = testing::Rectangle {
        width: 2,
        height: 1,
    };
    assert!(
        tall.intersects(&wide),
        "{}x{} should intersect {}x{}",
        tall.width,
        tall.height,
        wide.width,
        wide.height
    );
    assert!(wide.intersects(&tall));
}

#[test]
fn big_square_does_not_intersect_small_square() {
    let big = testing::Rectangle {
        width: 2,
        height: 2,
    };
    let small = testing::Rectangle {
        width: 1,
        height: 1,
    };
    assert!(!big.intersects(&small));
    assert!(!small.intersects(&big));
}

#[test]
fn equal_rectangles_intersect() {
    let first = testing::Rectangle {
        width: 1,
        height: 2,
    };
    let second = testing::Rectangle {
        width: 1,
        height: 2,
    };
    assert!(first.intersects(&second));
    assert!(second.intersects(&first));
}
