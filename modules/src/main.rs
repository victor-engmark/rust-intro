extern crate modules;

fn main() {
    modules::service::server::start();
    modules::service::client::connect();
    modules::client::connect();
}
