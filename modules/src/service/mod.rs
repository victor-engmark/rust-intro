pub mod client;
pub mod server;

#[cfg(test)]
mod tests {
    use super::server;

    #[test]
    fn it_works() {
        server::start();
    }
}
