use std::fmt::Display;

fn main() {
    let first = "foo";
    let second = "bar";

    print!("{} ", first);
    match max(first, second) {
        x if x == first => print!(">="),
        _ => print!("<="),
    }
    println!(" {}", second);

    let error = Error {
        message: "Han e dau!",
    };
    report(&error, "De e heilt feil!", "Farsken heller");
}

fn max<'a>(first: &'a str, second: &'a str) -> &'a str {
    if first >= second {
        first
    } else {
        second
    }
}

struct Error<'a> {
    message: &'a str,
}

fn report<'a, T>(error: &'a Error, prefix: &'a str, object: T) -> &'a str
where
    T: Display,
{
    println!("{} {}: {}", prefix, error.message, object);
    error.message
}
