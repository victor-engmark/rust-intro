use std::fmt::Debug;

fn main() {
    let mut weight = Weight::new(2);
    weight.set(3);
    weight.set(5);
    print_history(weight);
}

trait History<T: Copy> {
    fn get(&self) -> T;
    fn set(&mut self, value: T);
    fn history(&self) -> &Vec<T>;
}

impl History<u32> for Weight {
    fn get(&self) -> u32 {
        self.value
    }
    fn set(&mut self, value: u32) {
        self.history.push(self.value);
        self.value = value;
    }
    fn history(&self) -> &Vec<u32> {
        &self.history
    }
}

struct Weight {
    value: u32,
    history: Vec<u32>,
}

impl Weight {
    fn new(value: u32) -> Weight {
        Weight {
            value,
            history: Vec::new(),
        }
    }
}

fn print_history<T, U>(item: U)
where
    T: Copy + Debug,
    U: History<T>,
{
    println!(
        "Current value: {:?}. History: {:?}",
        item.get(),
        item.history()
    );
}
