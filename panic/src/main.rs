fn main() {
    //local();
    core();
}

fn local() {
    panic!("Goodbye, cruel world!");
}

fn core() {
    let values = vec![1, 2, 3];
    println!("{}", values[3]);
}
