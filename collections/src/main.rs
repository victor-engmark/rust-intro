extern crate rand;

use rand::Rng;

fn random_number() -> u64 {
    rand::thread_rng().gen_range(0, 10)
}

fn vectors() {
    let mut scores: Vec<u64> = Vec::new();
    scores.push(random_number());
    scores.push(random_number());
    println!("Scores: {:?}", scores);

    let weekdays = vec![1, 2, 3, 4, 5, 6, 7];
    let sunday = &weekdays[6];
    let octeday = weekdays.get(7);
    println!("Sunday: {}", sunday);
    println!("Octeday: {:?}", octeday);

    let mut temperatures = Vec::new();
    temperatures.push(99);
    temperatures.push(100);
    println!("Last temperature: {:?}", temperatures.pop());
}

fn string_creation() {
    let literal_to_string = "value".to_string();
    let string_from_literal = String::from("value");
    assert_eq!(literal_to_string, string_from_literal);

    let mut concatenated_string = String::new();
    concatenated_string.push_str("value");
    assert_eq!(string_from_literal, concatenated_string);

    let mut concatenated_character = String::from("valu");
    concatenated_character.push('e');
    assert_eq!(concatenated_string, concatenated_character);

    let va = String::from("va");
    let lue = String::from("lue");
    let plus_concatenated = va + &lue;
    assert_eq!(concatenated_character, plus_concatenated);
    // va has now moved, and is no longer available

    let first_name = "John";
    let middle_name = "Schrlu";
    let last_name = "Doe";
    let full_name = format!("{} {} {}", first_name, middle_name, last_name);
    println!("{}", full_name);
}

fn string_iteration() {
    for character in "नमस्ते".chars() {
        println!("{}", character);
    }
    for bytes in "नमस्ते".bytes() {
        println!("{}", bytes);
    }
}

fn main() {
    vectors();

    string_creation();
    string_iteration();
}
