fn main() {
    countdown();
    println!();
    iterate();
    println!();
    fibonacci(10);
}

fn countdown() {
    println!("Countdown");
    let mut number = 3;
    while number > 0 {
        println!("{}", number);
        number -= 1;
    }
}

fn iterate() {
    println!("Iteration");
    let numbers = [2, 1, 3];
    for number in numbers.iter() {
        println!("{}", number);
    }
}

fn fibonacci(count: i32) {
    println!("{} first Fibonacci numbers:", count);
    let mut fibs = [0, 1];
    for _ in 1..count {
        let sum = fibs[0] + fibs[1];
        println!("{}", sum);
        fibs = [fibs[1], sum];
    }
}
