extern crate rand;

use rand::Rng;

fn main() {
    let alive = run_experiment();
    if alive.is_some() {
        println!(
            "Is the cat alive? {}",
            alive.expect("PEBCAK error detected!")
        );
    } else {
        println!("Experiment failed!");
    }
}

fn run_experiment() -> Option<bool> {
    match rand::thread_rng().gen_range(0, 3) {
        0 => Some(true),
        1 => Some(false),
        _ => None,
    }
}
