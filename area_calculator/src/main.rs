#[derive(Debug)]
struct Rectangle {
    length: u32,
    width: u32,
}

#[derive(Debug)]
struct Line {
    length: u32,
}

impl Rectangle {
    fn compare_by_area(&self, other: &Rectangle) -> i8 {
        let self_area = self.area();
        let other_area = other.area();
        if self_area > other_area {
            return 1;
        } else if self_area < other_area {
            return -1;
        }
        0
    }
    fn area(&self) -> u32 {
        self.length * self.width
    }
    fn shrink(&mut self) {
        self.length = std::cmp::max(1, self.length - 1);
        self.width = std::cmp::max(1, self.width - 1);
    }
    fn flatten(self) -> Line {
        Line {
            length: self.length + self.width,
        }
    }
    fn square(size: u32) -> Rectangle {
        Rectangle {
            length: size,
            width: size,
        }
    }
}

fn main() {
    let mut first = Rectangle {
        length: 5,
        width: 7,
    };
    let second = Rectangle {
        length: 4,
        width: 10,
    };

    println!("The first area is {}", first.area());
    println!("The second area is {}", second.area());
    println!(
        "first compared to second is {}",
        first.compare_by_area(&second)
    );

    first.shrink();
    println!("After shrinking the rectangle is {:#?}", first);

    let line = first.flatten();
    println!("After destructive flattening it's a {:#?}", line);

    println!("A typical square: {:#?}", Rectangle::square(2));
}
