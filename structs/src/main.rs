struct User {
    email: String,
    active: bool,
    logins: u64,
}

struct DayNames(String, String, String, String, String, String, String);

fn main() {
    let email = String::from("anything@example.com");

    let mut me = create_user(email);
    me.email = String::from("me@example.com");

    let other = User {
        email: String::from("other@example.com"),
        ..me
    };

    let english_day_names = DayNames(
        String::from("Mehday"),
        String::from("Fussday"),
        String::from("Werkday"),
        String::from("Slogday"),
        String::from("Beerday"),
        String::from("Freeday"),
        String::from("Sleepday"),
    );
}

fn create_user(email: String) -> User {
    User {
        email: email,
        active: false,
        logins: 0,
    }
}
