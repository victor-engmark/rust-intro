fn main() {
    let value = Some(1u8);
    if let Some(42) = value {
        println!("For life!");
    } else {
        println!("Deeeath!");
    }
}
