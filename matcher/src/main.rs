use std::iter::Sum;

fn main() {
    let collection = vec![
        value(Coin::Krone(Shape::Hel)),
        value(Coin::Femmer(Shape::Hullete)),
        value(Coin::Tier),
        value(Coin::Tyvekroning),
    ];
    let sum: u32 = Sum::sum(collection.iter());
    println!("En myntsamling er {} kroner", sum);

    let slot_values = [None, Some(5), Some(20), None];
    println!("Sum plasser: {:?}", sum_slots(&slot_values));

    let word = String::from("shear");
    let count = 2;
    println!("{} × {} is {}", count, word, pluralise(&word, count));
}

fn sum_slots(slot_values: &[Option<u32>]) -> Option<u32> {
    let mut sum = 0;
    for slot_value in slot_values {
        sum += match slot_value {
            &None => 0,
            &Some(value) => value,
        }
    }
    match sum {
        0 => None,
        value => Some(value),
    }
}

#[derive(Debug)]
enum Shape {
    Hel,
    Hullete,
}

enum Coin {
    Krone(Shape),
    Femmer(Shape),
    Tier,
    Tyvekroning,
}

fn value(coin: Coin) -> u32 {
    match coin {
        Coin::Krone(shape) => {
            println!("{:?} krone", shape);
            1
        }
        Coin::Femmer(shape) => {
            println!("{:?} femmer", shape);
            5
        }
        Coin::Tier => 10,
        Coin::Tyvekroning => 20,
    }
}

fn pluralise(original: &str, number: u32) -> String {
    let mut pluralised = String::from(original);
    match original {
        "sheep" => String::from(original),
        _ => {
            match number {
                1 => (),
                _ => pluralised.push_str("s"),
            };
            pluralised
        }
    }
}
