extern crate num;

fn main() {
    let numbers = vec![3, 5, 2, 4];
    println!("Max number: {}", max(&numbers));

    let characters = vec!['r', 'u', 's', 't'];
    println!("Max character: {}", max(&characters));

    let strings = vec!["rust", "iron"];
    println!("Max string: {}", max(&strings));

    let floating_point_point = Point { x: 0.5, y: 3.1 };
    println!(
        "Integer x: {}; distance to ({}, {}): {}",
        Point { x: 1, y: 2 }.x(),
        floating_point_point.x(),
        floating_point_point.y(),
        floating_point_point.origin_vector_length()
    );
}

fn max<T: PartialOrd>(values: &[T]) -> &T {
    let mut max = &values[0];

    for value in values.iter() {
        if value > max {
            max = value;
        }
    }

    &max
}

struct Point<T> {
    x: T,
    y: T,
}

impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x
    }
    fn y(&self) -> &T {
        &self.y
    }
}

impl<T: num::Float> Point<T> {
    fn origin_vector_length(&self) -> T {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}
