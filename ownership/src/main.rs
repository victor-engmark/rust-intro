fn main() {
    println!("Copy demo");
    let x = 4;
    copy_demo(x);
    println!(
        "Stack values are copied on passing, so it's still available: {}",
        x
    );

    println!("\nMove demo");
    let input = String::from("in");
    move_demo(input);
    println!("Can't access heap variable after passing it");

    println!("\nClone demo");
    clone_demo();

    println!("\nReturn demo");
    let input = String::from("in");
    let output = return_demo(input);
    println!(
        "Can't access heap variable after passing it. We got '{}' back",
        output
    );

    println!("\nReference demo");
    let input = String::from("in");
    reference_demo(&input);
    println!(
        "Variable passed by reference is still available: '{}'",
        input
    );

    println!("\nMutable reference demo");
    mutable_reference_demo();
}

fn copy_demo(x: u32) {
    let y = x;

    println!("Stack values are copied on assignment: {} ≡ {}", x, y)
}

fn move_demo(input: String) {
    println!("You passed '{}'", input);
}

fn clone_demo() {
    let mut original = String::from("æ");
    original.push_str("øå");

    let last = original.pop().expect("Popping failed");

    // drop(secret); // Deallocate manually
    // let clone = original; // Move value
    let clone = original.clone();

    println!(
        "The original string '{}' is {} bytes and has {} bytes capacity after removing the last character, '{}'",
        original,
        original.len(),
        original.capacity(),
        last
    );
    println!(
        "The cloned string '{}' is {} bytes and has _{}_ bytes capacity",
        clone,
        clone.len(),
        clone.capacity()
    );
}

fn return_demo(input: String) -> String {
    input
}

fn reference_demo(input: &String) {
    println!("You passed a reference to '{}'", input);
}

fn mutable_reference_demo() {
    let mut original = String::from("original string");

    {
        let mutable_reference = &mut original;
        mutable_reference.push_str(" [inline scope modification]");
    }

    add_suffix(&mut original);

    let immutable_reference = &original;
    println!(
        "References in the original scope see the finished value: '{}'",
        immutable_reference
    );
}

fn add_suffix(input: &mut String) {
    input.push_str(" [function scope modification]")
}
