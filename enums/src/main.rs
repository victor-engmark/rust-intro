#[derive(Debug)]
struct Ipv4Address {
    address: u32,
}

impl Ipv4Address {
    fn from(input: &str) -> Ipv4Address {
        let mut address: u32 = 0;
        for (index, octet) in input.split('.').rev().enumerate() {
            let number: u32 = octet.parse().expect("Invalid octet");
            address += number << 8 * index;
        }
        Ipv4Address { address }
    }

    fn to_string(&self) -> String {
        format!(
            "{}.{}.{}.{}",
            self.address >> 24 & 0xff,
            self.address >> 16 & 0xff,
            self.address >> 8 & 0xff,
            self.address & 0xff,
        )
    }
}

#[derive(Debug)]
struct Ipv6Address {
    address_parts: [u16; 8],
}

impl Ipv6Address {
    fn from(input: &str) -> Ipv6Address {
        let mut address_parts = [0, 0, 0, 0, 0, 0, 0, 0];

        for (index, octet) in input.split(':').rev().enumerate() {
            if octet == "" {
                continue;
            }

            let octet = u16::from_str_radix(octet, 16).expect("Invalid octet");
            address_parts[7 - index] = octet;
        }
        Ipv6Address { address_parts }
    }

    fn to_string(&self) -> String {
        format!(
            "{:x}:{:x}:{:x}:{:x}:{:x}:{:x}:{:x}:{:x}",
            self.address_parts[0],
            self.address_parts[1],
            self.address_parts[2],
            self.address_parts[3],
            self.address_parts[4],
            self.address_parts[5],
            self.address_parts[6],
            self.address_parts[7],
        )
    }
}

#[derive(Debug)]
enum IpAddress {
    V4(Ipv4Address),
    V6(Ipv6Address),
}

impl IpAddress {
    fn to_string(&self) -> String {
        match *self {
            IpAddress::V4(ref address) => address.to_string(),
            IpAddress::V6(ref address) => address.to_string(),
        }
    }
}

fn main() {
    let loopback4 = IpAddress::V4(Ipv4Address::from("127.0.0.1"));
    println!(
        "IPv4 loopback ({}) in decimal is {:?}",
        loopback4.to_string(),
        loopback4
    );

    let loopback4 = IpAddress::V4(Ipv4Address::from("93.184.216.34"));
    println!(
        "IPv4 example.com ({}) in decimal is {:?}",
        loopback4.to_string(),
        loopback4
    );

    let loopback4 = IpAddress::V4(Ipv4Address::from("255.255.255.255"));
    println!(
        "IPv4 max ({}) in decimal is {:?}",
        loopback4.to_string(),
        loopback4
    );

    let loopback6 = IpAddress::V6(Ipv6Address::from("::1"));
    println!(
        "IPv6 loopback ({}) in decimal is {:?}",
        loopback6.to_string(),
        loopback6
    );

    let loopback6 = IpAddress::V6(Ipv6Address::from("2606:2800:220:1:248:1893:25c8:1946"));
    println!(
        "IPv6 example.com ({}) in decimal is {:?}",
        loopback6.to_string(),
        loopback6
    );

    let loopback6 = IpAddress::V6(Ipv6Address::from("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff"));
    println!(
        "IPv6 max ({}) in decimal is {:?}",
        loopback6.to_string(),
        loopback6
    );
}
