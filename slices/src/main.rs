fn main() {
    let original = String::from("First second third");

    let first_word = first_word(&original);
    println!("First word: '{}'", first_word)
}

fn first_word(input: &str) -> &str {
    let bytes = input.as_bytes();

    for (index, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &input[0..index];
        }
    }
    input
}
