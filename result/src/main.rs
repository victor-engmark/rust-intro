use std::fs::File;
use std::io;
use std::io::ErrorKind;
use std::io::Read;

fn main() {
    let path = "test.txt";
    open_file_with_create(path);
    println!(
        "First line: '{}'",
        read_line(path).expect("Could not read first line")
    );
}

fn open_file_with_create(path: &str) {
    match File::open(path) {
        Ok(_) => {
            println!("Opened existing file");
        }
        Err(ref error) if error.kind() == ErrorKind::NotFound => match File::create(path) {
            Ok(_) => println!("Created file"),
            Err(error) => panic!("Unrecoverable error when creating file: {:?}", error),
        },
        Err(error) => panic!("Unrecoverable error when opening file: {:?}", error),
    };
    File::open(path).unwrap();
    println!("Unwrapped file handle successfully");
    File::open(path).expect("Someone snuck in while I wasn't looking");
}

fn read_line(path: &str) -> Result<String, io::Error> {
    let mut line = String::new();

    File::open(path)?.read_to_string(&mut line)?;
    Ok(line)
}
