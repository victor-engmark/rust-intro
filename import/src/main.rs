enum PrimaryColor {
    Red,
    Green,
    Blue,
}

enum CMYK {
    Cyan,
    Magenta,
    Yellow,
    Black,
}

pub mod outer {
    pub mod inner {
        pub fn innie() {}
    }
}

use outer::inner;
use PrimaryColor::{Blue, Green};
use CMYK::*;

fn main() {
    inner::innie();

    let additive_color = Blue;
    let subtractive_color = Cyan;
}
