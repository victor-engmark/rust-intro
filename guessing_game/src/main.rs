extern crate rand;

use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    let secret = rand::thread_rng().gen_range(0, 100);

    loop {
        println!("Guess a number between 0 and 99!");

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(number) => number,
            Err(_) => {
                print!("Do you speak English in {}?! ", guess.trim());
                continue;
            }
        };

        print!("{} is ", guess);
        match guess.cmp(&secret) {
            Ordering::Less => println!("too low"),
            Ordering::Greater => println!("too high"),
            Ordering::Equal => {
                println!("exactly right!");
                break;
            }
        }
    }
}
